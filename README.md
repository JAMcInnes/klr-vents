# KLR Vents

STL files for my 3d printable KLR650 vents. Generation 1 (1987-2007). They look pretty close to stock, not perfect, but not bad! I recommend printing 'with supports' 'touching buildplate'. Makes the tabs better. Flex them into place using two hands.
